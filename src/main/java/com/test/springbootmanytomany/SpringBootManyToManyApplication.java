package com.test.springbootmanytomany;

import com.test.springbootmanytomany.entity.Post;
import com.test.springbootmanytomany.entity.Tag;
import com.test.springbootmanytomany.repository.PostRepository;
import com.test.springbootmanytomany.repository.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootManyToManyApplication implements CommandLineRunner {
	@Autowired
	PostRepository postRepository;

	@Autowired
	TagRepository tagRepository;

	public static void main(String[] args) {
		SpringApplication.run(SpringBootManyToManyApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Post post = new Post();
		post.setTitle("title");
		post.setContent("content");
		Tag sprigBoot = new Tag();
		sprigBoot.setName("spring");
		Tag hibernate =  new Tag();
		hibernate.setName("hinernate");


		post.getTags().add(sprigBoot);
		post.getTags().add(hibernate);


		sprigBoot.getPosts().add(post);
		hibernate.getPosts().add(post);
		this.postRepository.save(post);
	}
}
