package com.test.springbootmanytomany.repository;

import com.test.springbootmanytomany.entity.Post;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostRepository extends JpaRepository<Post , Integer> {
}
