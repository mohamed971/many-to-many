package com.test.springbootmanytomany.repository;

import com.test.springbootmanytomany.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TagRepository extends JpaRepository<Tag , Integer> {
}
